<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Team Giving Detail Page
 */

get_header(); ?>
<section class="two-col page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        <article class="container_boxed content_band">
	        	<div class="container__inner col__4">
	        		<?php the_field('left_content_area');?>
	        	</div>
	        	<div class="container__inner col__8">
	        		<?php the_field('right_content_area');?>
	        	</div>
	        </article>
			


			<aside class="container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
